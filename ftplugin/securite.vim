
" DESCRIPTION:  Securite filetype plugin file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Local settings

" encryption method
setlocal cryptmethod=blowfish2
" disable swap file
setlocal noswapfile
" disable undo
setlocal undolevels=-1
setlocal noundofile

" enable wrapping
setlocal wrap
" enable wrapping long lines at a character in 'breakat'
setlocal linebreak
" hide special symbols
setlocal nolist
" hide line numbers
setlocal norelativenumber
setlocal nonumber
" dsable syntax
setlocal syntax=off
" disable spelling
setlocal nospell

" Local mappings
nnoremap <buffer> <Leader>X :X<CR>
nnoremap <buffer> <Leader>x :x<CR>
nnoremap <buffer> <Leader>Z :call securite#securite#AESE()<CR>
nnoremap <buffer> <Leader>z :call securite#securite#AESD()<CR>
nnoremap <buffer> s :call securite#securite#SaveSecuriteBuff()<CR>
