
" DESCRIPTION:  Securite filetype commands file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Check if loaded
if exists("g:ft_securite_loaded")
    finish
endif
let g:ft_securite_loaded = 1

" Check dependencies
if has('cryptv')
    try
        set cryptmethod=blowfish2
    catch /^Vim\%((\a\+)\)\=:E474/
        echohl WarrnMsg
        echom "Please, note that your Vim installation doesn't support blowfish2 encryption"
    endtry
endif

if !executable('openssl')
    echohl ErrorMsg
    echom "openssl is not installed; the plugin will now exit"
    echohl None
    finish
endif

" Introduce commands
" creates a new temp securite buffer
command! -nargs=0 NewSecBuff call securite#securite#InitSecuriteBuff()
