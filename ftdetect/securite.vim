
" DESCRIPTION:  Securite filetype detection file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Init
autocmd! BufNew,BufNewFile,BufReadPre *.sec set filetype=securite

" Disable viminfo
autocmd! BufNew,BufNewFile,BufReadPre *.sec call securite#securite#ViminfoOff()

" Enable viminfo
autocmd! BufLeave,BufDelete *.sec call securite#securite#ViminfoOn()
