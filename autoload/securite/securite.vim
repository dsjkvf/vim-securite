
" DESCRIPTION:  Securite autoload plugin file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Init Securite buffer
function! securite#securite#InitSecuriteBuff()
    setlocal modifiable
    execute "edit .sec"
    setlocal buftype=nofile
    setlocal filetype=securite
endfunction

" Save Securite buffer
function! securite#securite#SaveSecuriteBuff()
    let par_name = input('Enter name: ', '', 'file')
    execute 'saveas ' . par_name
    bwipeout #
endfunction

" Encrypt with AES
function securite#securite#AESE()
    %s/\n/\#\#\#/
    execute "1,$!openssl aes-256-cbc -a -salt"
    call feedkeys("\<C-l>")
endfunction

" Decrypt with AES
function! securite#securite#AESD()
    execute "%!cat | openssl aes-256-cbc -a -d"
    %s/\#\#\#/\r/g
    $d
    call feedkeys("\<C-l>")
endfunction

" Save viminfo settings for a buffer (and disable viminfo itself)
function! securite#securite#ViminfoOff()
    let g:viminfo_orig = &viminfo
    let skip_defaults_vim=1
    set viminfo=
endfunction

" Restore viminfo settings
function! securite#securite#ViminfoOn()
    let skip_defaults_vim=0
    let &viminfo = g:viminfo_orig
endfunction
