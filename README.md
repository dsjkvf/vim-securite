Vim Securite
============

## About

This bundle introduces the `securite` filetype, which provides a simple workflow for encrypting and decrypting `*.sec` files without leaving any trace via `openssl` ([AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)).

However, on a personal note, I suggest you to use [gnupg.vim](http://www.vim.org/scripts/script.php?script_id=3645) plugin instead.

## Dependencies

Make sure `openssl` is installed (run `which openssl` in your shell).

## Mappings

The plugin introduces the following mappings for the `*.sec` files:

  - `<Leader>Z` will encrypt the current `securite` buffer;
  - `<Leader>z` will decrypt the current `securite` buffer;
  - `<Leader>X` will invoke the native Vim's encryption command `:help :X`;
  - `<Leader>x` will invoke the native Vim's decryption command `:help :x`;
  - `s` will prompt for a new name for the current `securite` buffer, will `saveas` it under that new name, and then will destroy it (writing temporary `securite` buffers to disk is not recommended due to security reasons).

## Installation

Copy the provided files to your `$VIMHOME` directory, or simply use any plugin manager available.

## License

Copyright (c) dsjkvf unless stated otherwise. Distributed under the same terms as Vim itself. See `:help license`.
